from flask import Flask
from flask_sqlalchemy import SQLAlchemy

app = Flask(__name__)
host='localhost'
db='task'
user='postgres'
password='cogilent123'
port = 5432
url = 'postgresql://{}:{}@{}:{}/{}'
url = url.format(user, password, host, port, db)
app.config['SQLALCHEMY_DATABASE_URI'] = url #'sqlite:////tmp/db_python.db'
db = SQLAlchemy(app)
class Companies(db.Model):
    id = db.Column(db.Integer, primary_key=True,autoincrement=True)
    name = db.Column(db.String(200), unique=True)
    city = db.Column(db.String(120), unique=False)
    address = db.Column(db.String(300), unique=False)
    created_at = db.Column(db.DateTime)
